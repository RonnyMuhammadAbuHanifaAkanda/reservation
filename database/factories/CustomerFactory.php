<?php

use Faker\Generator as Faker;

$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        "name" => $faker->alim,
        "email" => $faker->unique()->safeEmail,
        "phone" => $faker->phoneNumber,
        "address" => $faker->address,
    ];
});
