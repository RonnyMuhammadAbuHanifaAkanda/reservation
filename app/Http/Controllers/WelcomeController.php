<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Category;
use App\Item;

class WelcomeController extends Controller
{
    
    public function welcome()
    {
        $slider= Slider::all();
        $item  = Item::all();
        $category = Category::all();
        return view('welcome', compact('slider', 'item', 'category'));
    } 
}
