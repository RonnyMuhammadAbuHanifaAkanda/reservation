<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Reservation;

class ReservationController extends Controller
{
    public function reserve(Request $request)
    {
        $this->validate($request, [
            'reservation_name' =>'required',
            'phone' =>'required',
            'email' =>'required|email',
            'date_and_time' =>'required',
            'message' =>'required'
        ]);
        $reservation = new Reservation();
        $reservation->reservation_name = $request->reservation_name; 
        $reservation->phone = $request->phone; 
        $reservation->email = $request->email; 
        $reservation->date_and_time = $request->date_and_time; 
        $reservation->message = $request->message; 
        $reservation->status = false;
        $reservation->save();
         Toastr::success('Successfully sent! we will contact very soon', 'Hey Budy!!!', ["positionClass" => "toast-top-right"]);
         return redirect()->back();
    }

   
   
}
