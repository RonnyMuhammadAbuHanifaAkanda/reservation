<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Slider;
use Carbon\Carbon;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders =Slider::all(); 
        return view('slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('slider.create_slider');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required',
            'sub_title'=>'required',
            'image'=>'required|mimes:png,jpeg,jpg'
        ]);
        $image= $request->file('image');
        $slug= str_slug($request->title);
        if (isset($image))
        {
            $currentDate= Carbon::now()->toDateString();
            $imagename= $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if (!file_exists('uploads/slider')) 
            {
                mkdir('uploads/slider', 0777, true);
            }
                $image->move('uploads/slider', $imagename);
        }else{
            $imagename='default.png';
        }

        $slider = new Slider();
        $slider->title = $request->title;
        $slider->sub_title = $request->sub_title;
        $slider->image= $imagename;
        $slider->save();

        Toastr::success('Successfully sent! we will contact very soon', 'Hey Budy!!!', ["positionClass" => "toast-top-right"]);
         return redirect()->route('slider.index');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sliders=Slider::find($id);
        return view('slider.edit', compact('sliders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>'required',
            'sub_title'=>'required',
            'image'=>'mimes:png,jpeg,jpg',
        ]);
        $image= $request->file('image');
        $slug= str_slug($request->title);
        $slider=Slider::find($id);
        if (isset($image))
        {
            $currentDate= Carbon::now()->toDateString();
            $imagename= $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if (!file_exists('uploads/slider')) 
            {
                mkdir('uploads/slider', 0777, true);
                $image->move('uploads/slider', $imagename);
            }
        }else{
            $imagename=$slider->image;
        }

        $slider->title = $request->title;
        $slider->sub_title = $request->sub_title;
        $slider->image= $imagename;
        $slider->save();

        Toastr::success('Successfully sent! we will contact very soon', 'Hey Budy!!!', ["positionClass" => "toast-top-right"]);
         return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=Slider::find($id);
        if (file_exists('uploads/slider/'.$slider->image)) {
            
            unlink('uploads/slider/'.$slider->image);
        }
        $slider->delete();

        Toastr::success('Successfully sent! we will contact very soon', 'Hey Budy!!!', ["positionClass" => "toast-top-right"]);
         return redirect()->back();
    }
}
