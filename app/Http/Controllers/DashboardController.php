<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use DB;

class DashboardController extends Controller
{
    public function category()
    {
    	$category=Category::count();
    	return view('partial.content', compact('category'));
    }
}
