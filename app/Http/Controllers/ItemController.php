<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Category;
use Carbon\Carbon;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return view('item.item_index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
       return view('item.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'category_id'=>'required',
            'name'=>'required',
            'description'=>'required',
            'price'=>'required',
            'image'=>'required|mimes:png,jpeg,jpg',
        ]);
        $image= $request->file('image');
        $slug= str_slug($request->name);
        if (isset($image))
        {
            $currentDate= Carbon::now()->toDateString();
            $imagename= $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if (!file_exists('uploads/image')) 
            {
                mkdir('uploads/image', 0777, true);
            }
                $image->move('uploads/image', $imagename);
        }else{
            $imagename='default.png';
        }

        $item = new Item();
        $item->category_id = $request->category_id;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->price = $request->price;
        $item->image= $imagename;
        $item->save();

        return redirect()->route('item.index')->with('messageMsg','Saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::find($id);
        return view('partial.view_detail', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items=Item::find($id);
        $categories=Category::find($id);
        return view('item.edit', compact('items', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'=>'required',
            'description'=>'required',
            'price'=>'required',
            'image'=>'mimes:png,jpeg,jpg',
        ]);
        $item = Item::find($id);
        $image= $request->file('image');
        $slug= str_slug($request->name);
        if (isset($image))
        {
            $currentDate= Carbon::now()->toDateString();
            $imagename= $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if (!file_exists('uploads/image')) 
            {
                mkdir('uploads/image', 0777, true);
            }
                unlink('uploads/image/'.$item->image);
                $image->move('uploads/image', $imagename);
        }else{
            $imagename=$item->image;
        }

        
        $item->name = $request->name;
        $item->description = $request->description;
        $item->price = $request->price;
        $item->image= $imagename;
        $item->save();

        return redirect()->route('item.index')->with('messageMsg','Saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $item=Item::find($id);
        if (file_exists('uploads/image/'.$item->image)) {
            
            unlink('uploads/image/'.$item->image);
        }
        $item->delete();

        return redirect()->back()->with('messageMsg','Deleted successfully');
    }
}
