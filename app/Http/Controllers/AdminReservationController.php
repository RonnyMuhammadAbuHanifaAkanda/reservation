<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\ReservationConfirmed;
use Brian2694\Toastr\Facades\Toastr;
use App\Reservation;
use Illuminate\Support\Facades\Notification;

class AdminReservationController extends Controller
{
     public function index()
    {
        $reservation=Reservation::all();
        return view('reservation.index', compact('reservation'));
    }

     public function status($id)
    {
         $reserve = Reservation::find($id);
         $reserve->status = true;
         $reserve->save();
         Notification::route('mail', $reserve->email)
            // ->route('nexmo', '5555555555')for SMS
            ->notify(new ReservationConfirmed());

         Toastr::success('Successfully sent! we will contact very soon', 'Hey Budy!!!', ["positionClass" => "toast-top-right"]);
         return redirect()->back();
    }

    public function destroy($id)
    {
    	Reservation::find($id)->delete();
    	 Toastr::success('Successfully sent! we will contact very soon', 'Hey Budy!!!', ["positionClass" => "toast-top-right"]);
         return redirect()->back();
    }

    public function reservation_show($id)
    {
        $reservation=Reservation::find($id);
        return view('reservation.show', compact('reservation'));
    }
}
