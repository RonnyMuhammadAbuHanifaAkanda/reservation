<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Notification;
use App\Notifications\ContactNotification;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Contact;

class ContactController extends Controller
{
    public function sendMessage(Request $request)
    {
        $this->validate($request, [
            'name' =>'required',
            'email' =>'required|email', 
            'subject' =>'required',
            'message' =>'required'
        ]);
        $contact = new Contact();
        $contact->name = $request->name; 
        $contact->email = $request->email; 
        $contact->subject = $request->subject; 
        $contact->message = $request->message; 
        $contact->save();

        $contacts = Contact::all();
        foreach ($contacts as  $contacts) {
            Notification::route('mail', $contact->email)
            ->notify(new ContactNotification($contacts));
        }
         Toastr::success('Successfully sent! we will contact very soon', 'Hey Budy!!!', ["positionClass" => "toast-top-right"]);
         return redirect()->back();
    }
}
