@extends('admin.dashboard')

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

@endpush

@section('content')

    <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Reservation List</h4>
                                    <p class="category">Here is a subtitle for this table</p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table id="table" class="table table-striped" style="width:100%">
                                        <thead class="text-primary">
		                                    <th>ID</th>
		                                    <th>Name</th>
		                                    <th>Phone</th>
		                                    <th>Email</th>
		                                    <th>Send Time</th>
		                                    <th>Status</th>
		                                    <th>Action</th>
                                        </thead>
                                        <tbody>
                                            @foreach($reservation as $key=>$reserve)
		                                    <tr>
		                                       <td>{{ $key + 1 }}</td>
		                                       <td>{{ $reserve->reservation_name }}</td>
		                                       <td>{{ $reserve->phone }}</td>
		                                       <td>{{ $reserve->email }}</td>
		                                       <td>{{ $reserve->created_at }}</td>
		                                       <td>
		                                       		@if($reserve->status == true)
														<span class="label label-info">Confirm</span>
		                                       		@else
														<span class="label label-danger">Pending</span>
		                                       		@endif
		                                       </td>
		                                       <td>
		                                       	<a href="{{ URL::to('/show', $reserve->id) }}" class="btn btn-success btn-sm"><i class="material-icons">details</i></a>
		                                      @if($reserve->status == false)
													<form action="{{ route('reservation.status', $reserve->id) }}" style="display: none;" id="status-form-{{ $reserve->id }}" method="POST">
		                                       		@csrf
		                                       		</form>
		                                 
			                                       <button class="btn btn-success btn-sm" type="button" onclick="if (confirm('Are you sure want to Confirm this?')) {
			                                       		event.preventDefault();
			                                       		document.getElementById('status-form-{{ $reserve->id }}').submit();

			                                       } else {
			                                       	event.preventDefault();
			                                       }" ><i class="material-icons">done</i>
			                                       	
			                                       </button>
		                                      @endif
		                                       	<form action="{{ route('reservation.destroy', $reserve->id) }}" style="display: none;" id="delete-form-{{ $reserve->id }}" method="POST">
		                                       		@csrf
		                                       		@method('DELETE')
		                                       		
		                                       	</form>
		                                 
			                                       <button class="btn btn-danger btn-sm" type="button" onclick="if (confirm('Are you sure want to delete this?')) {
			                                       		event.preventDefault();
			                                       		document.getElementById('delete-form-{{ $reserve->id }}').submit();

			                                       } else {
			                                       	event.preventDefault();
			                                       }" ><i class="material-icons">delete</i>
			                                       	
			                                       </button>
		                                       </td>
		                                    </tr>
		                                    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
             

@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
    $('#table').DataTable();
} );
</script>

@endpush
