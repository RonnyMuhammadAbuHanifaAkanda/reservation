@extends('admin.dashboard')

@push('css')

@endpush

@section('content')

    <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Reservation Details</h4>
                                    <p class="category">Here is a subtitle for this table</p>
                                </div>
                                <div class="card-content">
                                	<div class="row">
                                		<div class="col-md-12">
                                            <strong>Name: {{ $reservation->reservation_name }}</strong><br>
                                            <strong>Email: {{ $reservation->phone }}</strong><br>
                                			<strong>Email: {{ $reservation->email }}</strong><br>
                                			<strong>Reserve Time: {{ $reservation->date_and_time }}</strong><br>
                                			<strong>Message:</strong><br><br>
                                			<p>{{ $reservation->message }}</p>
                                		</div>
                                	</div>
                        <a href="{{URL::to('/reservation')}}" class="btn btn-success">Back to Reservation List</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
             

@endsection

@push('scripts')


@endpush
