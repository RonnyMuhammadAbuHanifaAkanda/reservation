@extends('admin.dashboard')

@push('css')

@endpush

@section('content')

    <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Contact Message Details</h4>
                                    <p class="category">Here is a subtitle for this table</p>
                                </div>
                                <div class="card-content">
                                	<div class="row">
                                		<div class="col-md-12">
                                			<strong>Name: {{ $view->name }}</strong><br>
                                			<strong>Subject: {{ $view->subject }}</strong><br>
                                			<strong>Message:</strong><br><br>
                                			<p>{{ $view->message }}</p>
                                		</div>
                                	</div>
                        <a href="{{URL::to('/admin/contact')}}" class="btn btn-success">Back to Contact List</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
             

@endsection

@push('scripts')


@endpush
