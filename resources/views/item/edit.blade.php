@extends('admin.dashboard')

@push('css')


@endpush

@section('content')

    <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                           @include('partial.message')
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Add Item</h4>
                                    <p class="category">Here is a subtitle for this table</p>
                                </div>
                                <div class="card-content table-responsive">
                                    <form method="post" action="{{ route('item.update', $items->id) }}" enctype="multipart/form-data"> 
                                        @csrf
                                        @method('PUT')
                                        
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Name</label>
                                                    <input type="text" name="name" class="form-control" value="{{$items->name}}">
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Description</label>
                                                    <input type="text" name="description" class="form-control" value="{{$items->description}}"></input>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">price</label>
                                                    <input type="number" name="price" class="form-control" value="{{$items->price}}">
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-12">
                                                    <label class="control-label">Image</label>
                                                    <input type="file" name="image">
                                                </div>
                                            </div>
                                            <a href="{{ route('item.index') }}" class="btn btn-info">Back</a>
                                            <button type="submit" class="btn btn-primary">Save</button>        
                                        </div> 
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

@endsection

