@extends('admin.dashboard')

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

@endpush

@section('content')

    <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                        	<a href="{{ route('item.create') }}" class="btn btn-primary">Create New Item</a>
                        	@include('partial.message')
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">All Items</h4>
                                    <p class="itemegory">Here is a subtitle for this table</p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table id="table" class="table table-striped" style="width:100%">
                                        <thead class="text-primary">
		                                    <th>SL No</th>
		                                    <th>Category</th>
		                                    <th>Item Name</th>
		                                    <th>Image</th>
		                                    <th>Description</th>
		                                    <th>Price</th>
		                                    <th>Action</th>
                                        </thead>
                                        <tbody>
                                            @foreach($items as $key=>$item)
		                                    <tr>
		                                       <td>{{ $key +1 }}</td>
		                                       <td>{{ $item->category->name }}</td>
		                                       <td>{{ $item->name }}</td>
		                                       <td><img class="img-responsive img-thumbnail" src="{{ asset('uploads/image/'. $item->image)  }}" style="height: 100px; width: 100px;">
		                                       </td>
		                                       <td>{{ $item->description }}</td>
		                                       <td>{{ $item->price }}</td>
		                                       <td>
		                                       	<a href="{{ route('item.edit', $item->id ) }}" class="btn btn-success btn-sm"><i class="material-icons">edit</i></a>

		                                       	<form action="{{ route('item.destroy', $item->id) }}" style="display: none;" id="delete-form-{{ $item->id }}" method="POST">
		                                       		@csrf
		                                       		@method('DELETE')
		                                       		
		                                       	</form>
		                                 
			                                       <button class="btn btn-danger btn-sm" type="button" onclick="if (confirm('Are you sure want to delete this?')) {
			                                       		event.preventDefault();
			                                       		document.getElementById('delete-form-{{ $item->id }}').submit();

			                                       } else {
			                                       	event.preventDefault();
			                                       }" ><i class="material-icons">delete</i>
			                                       	
			                                       </button>
		                                       </td>
		                                    </tr>
		                                    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
    $('#table').DataTable();
} );
</script>

@endpush