<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><marquee>  Welcome To<strong> BURGER HOUSE</strong></marquee></a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                 <li class="dropdown">
                    <a  data-toggle="dropdown" href="#">
                        <i class="material-icons">web</i>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">Facebook Page</a>
                            <div class="divider"></div>
                            <a href="#">Website</a>
                           
                        </li>
                    </ul>
                </li>  
                <li class="dropdown">
                    <a  data-toggle="dropdown" href="#">
                        <i class="material-icons">settings</i>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">Company Profile</a>
                            <div class="divider"></div>
                            <a href="#">Status</a>                           
                        </li>
                    </ul>
                </li>  
                 
                <?php 
                    $contact=DB::table('contacts')->get();
                 ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">notifications</i>
                        <span class="notification">{{ $contact->count() }}</span>
                        <p class="hidden-lg hidden-md">Notifications</p>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach($contact as $con)
                        <li>
                            <a href="{{ route('contact.show', $con->id) }}">{{ $con->name }}</a>
                        </li>
                        @endforeach
                        
                    </ul>
                </li>                             
                <li class="dropdown">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="material-icons">supervisor_account  </i> {{ Auth::user()->name }}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">Profile</a>
                            <div class="divider"></div>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>  
            </ul>
        </div>
    </div>
</nav>