 @if ($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
             
        <div class="alert alert-danger">
            <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
            <span>
                <b> Danger - </b> {{ $error }}</span>
        </div>
            @endforeach
        </ul>
@endif

@if(session('messageMsg'))
    <div class="alert alert-success">
        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
        <span>
            <b> OK - </b> {{ session('messageMsg') }}</span>
    </div>
@endif