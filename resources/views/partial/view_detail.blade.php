<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="shortcut icon" href="{{asset('frontend/images/star.png')}}" type="favicon/ico" />  -->

        <title>Reservation</title>

        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/owl.theme.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/flexslider.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/pricing.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/toastr.min.css')}}">
    

        
        <script src="{{asset('frontend/js/jquery-1.11.2.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/jquery.flexslider.min.js')}}"></script>
     

    </head>
    <body data-spy="scroll" data-target="#template-navbar">

<section id="pricing" class="pricing">
            <div id="w">
                <div class="pricing-filter">
                    <div class="pricing-filter-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="section-header">
                                        <h2 class="pricing-title">Have A Look To Dishe</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
               
                    </div>
                </div>      
            </div> 
        </section>
         <section id="have-a-look" class="have-a-look hidden-xs">
            <img class="img-responsive section-icon hidden-sm hidden-xs" src="">
            <div class="wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="menu-gallery" style="width: 50%; float:left;">
                            <div class="flexslider-container">
                                <div class="flexslider">
                                    <img src="{{asset('uploads/image/'.$item->image)}}" class="img-responsive" alt="Food" style="height: 685px; width: 743px; margin-top: 5px;">
                                </div>
                            </div>
                        </div>
                       <div class="col-xs-12 col-sm-6 dis-table-cell">
                            <div class="section-content">
                                <h1><b>Menu Details</b> </h1>
                                <h3 class="section-content-para">
                                   <b> Menu Name :</b> {{ $item->name  }}
                                </h3>
                                <h4 class="section-content-para">
                                    <b> Details :</b> {{ $item->description  }}.jkjkjlksaklkdjfkdjfkd djfdjf df fjdhfjd fjdf uhfds yue yui uiyuifsyidf uisfdsuiy fu oi oauidyf uiaoiuyduiyfuiaoiu uiui  iuy a uiuifiuayofuidy fuiaiudfyiuadfuiaiudsfuiyiuasdyfdayfu auifyuid yfuiuidyfuiyauidyfuiaduifyadufuiaydfuiyadf udyf  uidyfuid fuidyf iuy fuiaf 
                                </h4>
                                <br><br><br>
                                <a href="{{ URL::to('/') }}" class="btn btn-success btn-sm">Back to Main Page</a>
                            </div>

                        </div>
                        <footer>
                             <div class="container">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="copyright text-center">
                                            <p>
                                                &copy; Copyright, 2015 <a href="#">Your Website Link.</a> Theme by <a href="http://themewagon.com/"  target="_blank">ThemeWagon</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </div> <!-- /.row -->
                </div> <!-- /.container-fluid -->
            </div> <!-- /.wrapper -->
        </section>
    
        <script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/jquery.mixitup.min.js')}}" ></script>
        <script src="{{asset('frontend/js/wow.min.js')}}"></script>
        <script src="{{asset('frontend/js/jquery.validate.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/jquery.hoverdir.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/jQuery.scrollSpeed.js')}}"></script>
        <script src="{{asset('frontend/js/script.js')}}"></script>
        <script src="{{asset('frontend/js/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{asset('frontend/js/toastr.min.js')}}"></script>       

    </body>
</html>
