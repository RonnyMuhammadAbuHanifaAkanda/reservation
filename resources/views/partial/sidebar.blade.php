<div class="sidebar" data-color="purple" data-image="{{asset('backend/img/sidebar-1.jpg')}}">
                <div class="logo">
                    <a href="#" class="simple-text">
                        <strong> BURGER HOUSE</strong>
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="{{ Request::is('home*') ? 'active': '' }}">
                            <a href="{{ route('home') }}">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="{{ Request::is('slider*') ? 'active': '' }}">
                            <a href="{{ route('slider.index') }}">
                                <i class="material-icons">slideshow</i>
                                <p>All sliders</p>
                            </a>
                        </li>
                        <li class="{{ Request::is('category*') ? 'active': '' }}">
                            <a href="{{route('category.index')}}">
                                <i class="material-icons">view_list</i>
                                <p>All Categories</p>
                            </a>
                        </li>
                        <li class="{{ Request::is('item*') ? 'active': '' }}">
                            <a href="{{route('item.index')}}">
                                <i class="material-icons">view_list</i>
                                <p>All Items</p>
                            </a>
                        </li>
                        <li class="{{ Request::is('reservation*') ? 'active': '' }}">
                            <a href="{{URL::to('/reservation')}}">
                                <i class="material-icons">work</i>
                                <p>Reservation</p>
                            </a>
                        </li>
                        <li class="{{ Request::is('admin/contact*') ? 'active': '' }}">
                            <a href="{{URL::to('/admin/contact')}}">
                                <i class="material-icons">message</i>
                                <p>Contact Message</p>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </div>