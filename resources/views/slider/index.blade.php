@extends('admin.dashboard')

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

@endpush

@section('content')

    <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                        	<a href="{{ route('slider.create') }}" class="btn btn-primary">Create New Slider</a>
                        	@include('partial.message')
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Simple Table</h4>
                                    <p class="category">Here is a subtitle for this table</p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table id="table" class="table table-striped" style="width:100%">
                                        <thead class="text-primary">
		                                    <th>ID</th>
		                                    <th>Title</th>
		                                    <th>Sub Title</th>
		                                    <th>Image</th>
		                                    <th>Action</th>
                                        </thead>
                                        <tbody>
                                            @foreach($sliders as $key=>$slider)
		                                    <tr>
		                                       <td>{{ $key + 1 }}</td>
		                                       <td>{{ $slider->title }}</td>
		                                       <td>{{ $slider->sub_title }}</td>
		                                       <td>
												<img class="img-responsive img-thumbnail" src="{{ asset('uploads/slider/'. $slider->image)  }}" style="height: 100px; width: 100px;">
		                                       	</td>
		                                       <td>
		                                       	<a href="{{ route('slider.edit', $slider->id ) }}" class="btn btn-success btn-sm"><i class="material-icons">edit</i></a>

		                                       	<form action="{{ route('slider.destroy', $slider->id) }}" style="display: none;" id="delete-form-{{ $slider->id }}" method="POST">
		                                       		@csrf
		                                       		@method('DELETE')
		                                       		
		                                       	</form>
		                                 
			                                       <button class="btn btn-danger btn-sm" type="button" onclick="if (confirm('Are you sure want to delete this?')) {
			                                       		event.preventDefault();
			                                       		document.getElementById('delete-form-{{ $slider->id }}').submit();

			                                       } else {
			                                       	event.preventDefault();
			                                       }" ><i class="material-icons">delete</i>
			                                       	
			                                       </button>
		                                       </td>
		                                    </tr>
		                                    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
    $('#table').DataTable();
} );
</script>

@endpush