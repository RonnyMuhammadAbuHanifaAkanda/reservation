<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Auth::routes();

Route::get('/', 'WelcomeController@welcome')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/reservation', 'ReservationController@reserve')->name('reservation.reserve');
Route::get('/reservation', 'AdminReservationController@index');
Route::get('/show/{id}', 'AdminReservationController@reservation_show');
Route::post('/reservation/{id}', 'AdminReservationController@status')->name('reservation.status');
Route::delete('/reservation/{id}', 'AdminReservationController@destroy')->name('reservation.destroy');
Route::post('/contact', 'ContactController@sendMessage')->name('contact.send');
Route::get('/dashboard/category', 'DashboardController@category');
Route::get('/view-details/{id}', 'ViewDetailsController@view_details');



Route::resource('/slider', 'SliderController');
Route::resource('/category', 'CategoryController');
Route::resource('/item', 'ItemController');
Route::resource('/admin/contact', 'AdminContactController');
Route::resource('/new', 'NewController');
